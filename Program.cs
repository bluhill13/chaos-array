﻿using System;
using System.Threading.Tasks.Dataflow;

namespace Chaos_Array
{
    class Program
    {
        public static ChaosArray chaosArray = new ChaosArray();
        static void Main(string[] args)
        {
            chaosArray.InsertItem(23);
            chaosArray.InsertItem(new ChaosArray());
            chaosArray.InsertItem("hello");
            chaosArray.InsertItem("koman");
            chaosArray.InsertItem(3);
            chaosArray.InsertItem(100000123123);
            chaosArray.InsertItem(new UnableToInsertNewObjectExeption());
            chaosArray.InsertItem(new NoValueAtIndexExeption());
            do
            {
                Console.WriteLine("\t\nWelcome to Chaos-Array\n");
                Console.WriteLine("\tOptions: '1' Add item / '2' Get random item / 'exit' to exit program\n");
                Console.Write("Write an option and hit 'Enter': ");
                string input = Console.ReadLine();
                Menu(input);
                if (input == "exit") break;
            } while (true);
            

            Console.ReadKey();
        }
        /// <summary>
        /// Validates and guides the user to add or get a item from the list. 
        /// </summary>
        /// <param name="input"></param>
        public static void Menu(string input)
        {
            try
            {
                int choice = 0;
                if (IsDigitsOnly(input)) choice = Int32.Parse(input);
                switch (choice)
                {
                    case 1:
                        Console.Write("Write a number or a word to add to the ChaosArray: ");
                        string addition = Console.ReadLine();
                        if (IsDigitsOnly(addition)) chaosArray.InsertItem(Int32.Parse(addition));
                        else chaosArray.InsertItem(addition);
                        break;
                    case 2:
                        Console.WriteLine($"This is the randomly choosen object: {chaosArray.GetItem()}"); 
                        break;
                    default:
                        Console.WriteLine("Wrong input try again!");
                        break;
                }
            }
            catch (FormatException ex)
            {
                Console.WriteLine("You have written a too large number!");
            }
        }

        /// <summary>
        /// Validates that string only have numbers in it. 
        /// </summary>
        /// <param name="str">user input as string</param>
        /// <returns>false if there is non numbers present in input string</returns>
        static bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }
    }
}
