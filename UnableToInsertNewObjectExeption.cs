﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chaos_Array
{
    public class UnableToInsertNewObjectExeption : Exception
    {
        public int Index { get; set; }
        public UnableToInsertNewObjectExeption()
        {

        }

        public UnableToInsertNewObjectExeption(int index) : base(String.Format($"Index: {index} has already been used"))
        {
            Index = index;
        }
    }
}
