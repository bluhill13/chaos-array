﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Chaos_Array
{
    public class ChaosArray : ICollection
    {

        /// <summary>
        /// the array containing all possible types
        /// </summary>
        public object[] randomData = new object[] { null, null, null, null, null, null, null, null, null, null};

        /// <summary>
        /// Inserts a object at a random index in randomData object array.
        /// </summary>
        /// <param name="o"></param>
        public void InsertItem(Object o)
        {
            try
            {   
                //If validated it will set a new value, else throws exception
                randomData.SetValue(o, ValidateIndex(RandomNumbGenerator(Count), true));
            }
            catch (UnableToInsertNewObjectExeption ex) {
                //Ask to overwrite or find an unused spot
                Console.Write($"{ex.Message}, do you want to overwrite? ('y' = yes / anything for no)");
                string input = Console.ReadLine();
                if (input == "y" || input == "Y") randomData.SetValue(o, ex.Index);
                else
                {
                    Console.WriteLine("Going backwards to find a unused index");
                    for (int i = Count - 1; i >= 0; i--)
                    {
                        if (randomData.GetValue(i) == null) 
                        { 
                            randomData.SetValue(o, i);
                            Console.WriteLine($"Set value at {i}");
                            break;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Gets a random item/object in the chaos array or gives you an error message if nothing is present
        /// </summary>
        /// <returns></returns>
        public object GetItem() 
        {
            try
            {
                //If validation fails it will throw custom exeption below
                return randomData.GetValue(ValidateIndex(RandomNumbGenerator(Count), false));
            }catch(NoValueAtIndexExeption ex)
            {
                Console.WriteLine($"{ex.Message}");
                for (int i = 0; i < Count; i++)
                {
                    if (randomData.GetValue(i) != null) {
                        var temp = randomData.GetValue(i);
                        //Set data to null when removing?
                        randomData.SetValue(null, i);
                        return temp;
                    }
                }
                Console.WriteLine("There is nothing in the chaos array!");
                return null;
            }
        }
        /// <summary>
        /// Validates if the randomly selected index already have data or is empty, depending on the bool "insert"
        /// </summary>
        /// <param name="i"></param>
        /// <param name="insert"></param>
        /// <returns></returns>
        public int ValidateIndex(int i, bool insert)
        {
            if (insert)
            {
                if (randomData[i] == null) return i;
                else throw new UnableToInsertNewObjectExeption(i);
            }
            else
            {
                if (randomData[i] != null) return i;
                else throw new NoValueAtIndexExeption(i);
            }
        }
       /// <summary>
       /// Generates a random number between 0 and "limit"
       /// </summary>
       /// <param name="limit"></param>
       /// <returns></returns>
        public int RandomNumbGenerator(int limit)
        {
            int value = RandomNumberGenerator.GetInt32(limit);
            return value;
        }

        public int Count => randomData.Length;

        public object SyncRoot => throw new NotImplementedException();

        bool ICollection.IsSynchronized => randomData.IsSynchronized;

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
