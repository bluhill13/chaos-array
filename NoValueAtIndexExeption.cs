﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chaos_Array
{
    class NoValueAtIndexExeption : Exception
    {
        public NoValueAtIndexExeption()
        {

        }
        public NoValueAtIndexExeption(int i) : base (String.Format($"There was no value at index: {i}"))
        {

        }
    }
}
